# docker build --compress -t sl7 -f Dockerfile.rinse .

### build SL7 chroot using Debian Stretch
FROM debian:stretch as chrootbuilder
WORKDIR /root

RUN \
  apt-get update ; apt-get -y install rinse xz-utils tar

# add SL7 to config file
RUN \
  echo "[scientificlinux-7]" \
    >> /etc/rinse/rinse.conf ;\
  echo "mirror.amd64 = http://ftp.scientificlinux.org/linux/scientific/7/x86_64/os/Packages/" \
    >> /etc/rinse/rinse.conf ;\
  sed 's/centos-release/sl-release/g' < \
    /etc/rinse/centos-7.packages > /etc/rinse/scientificlinux-7.packages ;\
  echo yum-conf-sl7x >> /etc/rinse/scientificlinux-7.packages ;\
  echo yum-conf-repos >> /etc/rinse/scientificlinux-7.packages ;\
  echo pyliblzma >> /etc/rinse/scientificlinux-7.packages ;\
  echo gzip >> /etc/rinse/scientificlinux-7.packages ;\
  echo systemd >> /etc/rinse/scientificlinux-7.packages ;\
  echo python-kitchen >> /etc/rinse/scientificlinux-7.packages ;\
  echo ca-certificates >> /etc/rinse/scientificlinux-7.packages ;\
  echo nss-pem >> /etc/rinse/scientificlinux-7.packages ;\
  echo nss-tools >> /etc/rinse/scientificlinux-7.packages ;\
  echo openssl >> /etc/rinse/scientificlinux-7.packages ;\
  echo p11-kit >> /etc/rinse/scientificlinux-7.packages ;\
  echo libtasn1 >> /etc/rinse/scientificlinux-7.packages ;\
  echo p11-kit-trust >> /etc/rinse/scientificlinux-7.packages

# kick off rinse with our fresh SL7 config
RUN \
  rinse \
    --distribution scientificlinux-7 \
    --directory /root/sl7-chroot \
    --arch amd64 \
    --cache --cache-dir /root/rinse-cache && \
  rm /root/sl7-chroot/*.rpm /root/sl7-chroot/etc/resolv.conf ;\
  rm -fr /root/var/cache/yum/*

#### build SL7 docker image from the generated chroot
FROM scratch
WORKDIR /

# all this extra goop because you cannot just do this:
#ADD --from=chrootbuilder /root/sl7-amd64.tar.xz /

COPY --from=chrootbuilder /root/sl7-chroot/ /

# Fix NSS being _really_ broken

RUN \
  sed s/libnsssysinit\.so/libnsspem.so/g \
    < /etc/pki/nssdb/pkcs11.txt \
    > /etc/pki/nssdb/pkcs11.txt.new && \
  mv /etc/pki/nssdb/pkcs11.txt.new /etc/pki/nssdb/pkcs11.txt ;\
  update-ca-trust extract

RUN \
  yum -y upgrade ; yum -y erase plymouth* ; yum clean all ;\
  rm -fr /var/cache/yum

COPY post.sh /root/post.sh
RUN \
  bash /root/post.sh ;\
  rm /root/post.sh

# for mock
COPY epel-7-x86_64.cfg /

CMD /bin/bash
